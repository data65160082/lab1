/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1;

import java.util.Arrays;

/**
 *
 * @author Tauru
 */
public class MergeSortedArray {

    public static void main(String[] args) {
        int n = 3;
        int m = 3;
        int[] nums1;
        nums1 = new int[m + n];
        nums1[0] = 1;
        nums1[1] = 2;
        nums1[2] = 3;
        int[] nums2;
        nums2 = new int[n];
        nums2[0] = 2;
        nums2[1] = 5;
        nums2[2] = 6;
        
        merge(nums1, m, nums2, n);
        

        for (int num : nums1) {
            System.out.print(num + " ");
        }
        }
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int p1 = m - 1; 
        int p2 = n - 1; 
        int p = m + n - 1;
        
        while (p1 >= 0 && p2 >= 0) {
            if (nums1[p1] > nums2[p2]) {
                nums1[p] = nums1[p1];
                p1--;
            } else {
                nums1[p] = nums2[p2];
                p2--;
            }
            p--;
        }

        // If there are remaining elements in nums2, copy them to nums1
        while (p2 >= 0) {
            nums1[p] = nums2[p2];
            p2--;
            p--;    
        }
    }
}
