/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1;

import java.util.Arrays;

/**
 *
 * @author Tauru
 */
public class DuplicateZeros {
    
    public static void main(String[] args) {
        int[] arr = {1,0,2,3,0,4,5,0};
        System.out.println("Original Array : "+Arrays.toString(arr));
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                for (int j = arr.length - 1; j > i; j--) {
                    arr[j] = arr[j - 1];                    
                }
                i = i + 1;                
            }
            
        }
        System.out.print("Duplicated Zeros Array : "+Arrays.toString(arr));
    }
}
